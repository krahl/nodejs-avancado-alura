module.exports  = function(app){
	app.get("/pagamentos", function(req, res){
		console.log("REquisição de tese");
		res.send("OK");
	});

	app.post("/pagamentos/pagamento", function(req,res){
		var pagamento = req.body;
		console.log("Processando requisicao de pagamento");

		req.assert("forma_de_pagamento", "Forma de pagamento é obrigatorio").notEmpty();
		req.assert("valor", "Valor é obrigatório e deve ser um decimal").notEmpty().isFloat();

		var erros = req.validationErros();

		if(erros){
			console.log("Erros de validação encontrados");
			res.status(400).send(erros);
			return;
		}

		pagamento.status = 'CRIADO';
		pagamento.data = new Date();

		var connection = app.persistencia.ConnectionFactory();
		var pagamentoDao = new app.persistencia.PagamentoDao(connection);

		pagamentoDao.salva(pagamento, function(erro, resultado){
			if(erro){
				console.log("Falha ao criar o registro");
				res.status(500).send(erro);
			}else{
				console.log('pagamento criado');
				res.location("/pagamentos/pagamento/"+resultado.insertId);
				res.status(201).json(pagamento);
			}
		});
	});
}